# ---------------------------
# RHEL packaging workflow
# ---------------------------

include:
  # https://computing.docs.ligo.org/gitlab-ci-templates/
  - project: computing/gitlab-ci-templates
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/
    file: rhel.yml
  # local test template
  - local: /.gitlab/ci/test.yml

# -- macros

.el7:
  image: igwn/builder:el7-testing
  variables:
    EPEL: "true"

.el8:
  image: igwn/builder:el8-testing
  variables:
    EPEL: "true"

# -- source packages --------
#
# These jobs make src RPMs
#

.srpm:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:srpm
    - .rhel:srpm
  stage: source packages
  needs:
    - tarball
  variables:
    TARBALL: "gwdatafind-server-*.tar.*"

srpm:el7:
  extends:
    - .srpm
    - .el7

srpm:el8:
  extends:
    - .srpm
    - .el8

# -- binary packages --------
#
# These jobs generate binary RPMs
# from the src RPMs
#

.rpm:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:rpm
    - .rhel:rpm
  stage: binary packages
  variables:
    SRPM: "python-gwdatafind-server-*.src.rpm"

rpm:el7:
  extends:
    - .rpm
    - .el7
  needs:
    - srpm:el7

rpm:el8:
  extends:
    - .rpm
    - .el8
  needs:
    - srpm:el8

# -- test -------------------

.test:el:
  extends:
    # see /.gitlab/ci/test.yml
    - .test
  before_script:
    # set up yum caching
    - !reference [".rhel:base", before_script]
    # configure EPEL
    - yum -y -q install epel-release && yum -y -q install epel-rpm-macros
    # install testing dependencies
    - PY3=$(rpm --eval '%{?python3_pkgversion:%{python3_pkgversion}}%{!?python3_pkgversion:3}')
    - yum -y -q install
          findutils
          python${PY3}-coverage
          python${PY3}-pytest
          python${PY3}-pytest-cov
    # install our package(s)
    - yum -y -q install *.rpm

test:el7:
  extends:
    - .test:el
    - .el7
  needs:
    - tarball
    - rpm:el7
  image: igwn/base:el7-testing

# -- EL8 has python3-flask < 1.0.0
#test:el8:
#  extends:
#    - .test:el
#    - .el8
#  needs:
#    - tarball
#    - rpm:el8

# -- lint -------------------
#
# These jobs check the code
# for quality issues
#

.rpmlint:
  extends:
    # https://computing.docs.ligo.org/gitlab-ci-templates/rhel/#.rhel:lint
    - .rhel:lint
  stage: code quality

rpmlint:el7:
  extends:
    - .rpmlint
    - .el7
  needs:
    - rpm:el7

rpmlint:el8:
  extends:
    - .rpmlint
    - .el8
  needs:
    - rpm:el8
