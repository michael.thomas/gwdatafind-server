# -*- coding: utf-8 -*-
# Copyright (2022) Cardiff University
# Licensed under GPLv3+ - see LICENSE

"""GWDataFind API
"""

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"

DEFAULT_API = "v1"
